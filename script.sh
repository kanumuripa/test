#!/bin/bash

# Get the pipeline variable
ACCOUNT_NAME=$(echo "$account_name")
EMAIL_DL=$(echo "$email_dl")
BILLING_OWNER_NAME=$(echo "$billing_owner_name")
BILLING_EMAIL=$(echo "$billing_email_dl")
BILLING_TITLE=$(echo "$billing_title")
OPERATIONS_OWNER_NAME=$(echo "$operations_owner_name")
OPERATIONS_EMAIL_DL=$(echo "$operations_email_dl")
OPERATIONS_TITLE=$(echo "$operations_title")
SECURITY_OWNER_NAME=$(echo "$security_owner_name")
SECURITY_EMAIL_DL=$(echo "$security_email_dl")
SECURITY_TITLE=$(echo "$security_title")
PHONE_NUMBER=$(echo "$phone_number")
ROLE_NAME=$(echo "$role_name") 


#echo $EMAIL_DL
#echo $PHONE_NUMBER
#echo $ACCOUNT_NAME

# GitLab API variables
GITLAB_API_URL="https://gitlab.com/api/v4"
PROJECT_ID="52151715"
REPO_PATH="kanumuripa/test"
BRANCH_NAME="main"
FILE_PATH="env/$ACCOUNT_NAME.tfvars"
BACKEND_PATH="env/$ACCOUNT_NAME.tfvars"
ACCESS_TOKEN=$GIT_TOCKEN

git checkout main
#git branch
# Create or update the file
echo "account_name = \"$ACCOUNT_NAME\"" > env/$ACCOUNT_NAME.tfvars
echo "email_dl = \"$EMAIL_DL\"" >> env/$ACCOUNT_NAME.tfvars
echo "billing_owner_name = \"$BILLING_OWNER_NAME\"" >> env/$ACCOUNT_NAME.tfvars
echo "billing_email_dl = \"$BILLING_EMAIL\"" >> env/$ACCOUNT_NAME.tfvars
echo "billing_title = \"$BILLING_TITLE\"" >> env/$ACCOUNT_NAME.tfvars
echo "operations_owner_name = \"$OPERATIONS_OWNER_NAME\"" >> env/$ACCOUNT_NAME.tfvars
echo "operations_email_dl = \"$OPERATIONS_EMAIL_DL\"" >> env/$ACCOUNT_NAME.tfvars
echo "operations_title = \"$OPERATIONS_TITLE\"" >> env/$ACCOUNT_NAME.tfvars
echo "security_owner_name = \"$SECURITY_OWNER_NAME\"" >> env/$ACCOUNT_NAME.tfvars
echo "security_email_dl = \"$SECURITY_EMAIL_DL\"" >> env/$ACCOUNT_NAME.tfvars
echo "security_title = \"$SECURITY_TITLE\"" >> env/$ACCOUNT_NAME.tfvars
echo "phone_number = \"$PHONE_NUMBER\"" >> env/$ACCOUNT_NAME.tfvars
echo "role_name = \"$ROLE_NAME\"" >> env/$ACCOUNT_NAME.tfvars

cat env/$ACCOUNT_NAME.tfvars

echo "bucket = \"charterprod-terraform-state\"" > backup/$ACCOUNT_NAME.tfvars
echo "region = \"us-east-1\"" >> backup/$ACCOUNT_NAME.tfvars
echo "key = \"subaccount/$ACCOUNT_NAME/terraform.state\"" >> backup/$ACCOUNT_NAME.tfvars

cat backup/$ACCOUNT_NAME.tfvars

#ls -lart
#pwd
#git status
#git branch
# Commit and push the changes
git config --global user.email "kanumuripa@gmail.com"
git config --global user.name "kanumuripa"
git remote set-url origin https://kanumuripa:$ACCESS_TOKEN@gitlab.com/kanumuripa/test.git
git add env/$ACCOUNT_NAME.tfvars
git add backup/$ACCOUNT_NAME.tfvars
git commit -m "Update $ACCOUNT_NAME.tfvars from pipeline"
git push
